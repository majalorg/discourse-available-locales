# name: discourse-available-locales
# about: A Discourse plugin to limit available locales shown in the Users' Interface preferences
# version: 0.0.1
# authors: Ahwaa Team
# url: https://gitlab.com/majalorg/discourse-available-locales

after_initialize do
  if SiteSetting.allow_user_locale
    class ::SiteSetting
      module SiteSettingExtensions
        def available_locales
          LocaleSiteSetting.values.select do |locale|
            available_interface_locales.include?(locale[:value])
          end.to_json
        end
      end
      singleton_class.prepend SiteSettingExtensions
    end
  end
end
