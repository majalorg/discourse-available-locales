# Discourse Available Locales

A Discourse plugin to limit the options in the Interface language
dropdown.

## Installation

Edit your web template and add the project clone url. Then, rebuild your web container so that the plugin installs. Check out the [official guide](https://meta.discourse.org/t/install-a-plugin/19157) for more info.

```sh
- git clone https://gitlab.com/majalorg/discourse-available-locales
```

### Enable plugin

This plugin is only enabled if the **allow user locale** setting is enabled
